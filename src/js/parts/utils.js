import jQuery from 'jquery';
import moment from 'jalali-moment';

const $popupDiv = document.getElementById('popup');

export const persianText = (value) => {
    const persianCharacters = ['ا', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی'];
    let letter;
    for (let i = 0; i < value.length; i++) {
        letter = persianCharacters.find(cur => cur === value[i]);
    }
    if (letter) {
        return true;
    } else {
        return false;
    }
};

export const popupFunctionality = () => {
    const $popups = document.querySelectorAll('.popup');
    const popupsArr = Array.prototype.slice.call($popups);

    popupsArr.forEach(el => {
        [el.querySelector('.popup__backdrop'), el.querySelector('.popup__close-btn')].forEach(cur => {
            cur.addEventListener('click', () => {
                el.querySelector('.popup__container').style.animation = 'popOut .3s';
                el.querySelector('.popup__container').style.animationFillMode = 'forwards';
                jQuery(el).fadeOut(300, () => {
                    jQuery(el).remove();
                });
            });
        });

        const $fileChoose = el.querySelector('.form__file-input label');
        if ($fileChoose) {
            $fileChoose.addEventListener('mousedown', () => {
                $fileChoose.style.transform = 'translateY(1px)';
            });
            $fileChoose.addEventListener('mouseup', () => {
                $fileChoose.style.transform = 'translateY(-1px)';
            });
        }
    });
};

export const cardFunctionality = () => {
    const cards = document.querySelectorAll('.card');
    const cardsArr = Array.prototype.slice.call(cards);
    cardsArr.forEach(card => {
        card.addEventListener('mousedown', (e) => {
            if (e.target.matches('.card__img, .card__author *')) {
                card.classList.add('card--clicked');
            }
            card.addEventListener('mouseleave', (e) => {
                card.classList.remove('card--clicked');
            });
            
        });
    })
};

export const renderLoader = () => {
    const loader = `
        <div class="popup">
            <div class="popup__backdrop"></div>
            <div style="padding: 4rem 0;" class="popup__container">
                <div class="loader">Loading...</div>
            </div>
        </div>
    `;
    $popupDiv.insertAdjacentHTML('beforeend', loader);
};

export const renderMessage = (message, btnText) => {
    let btn = '<div></div>';
    if (btnText) {
        btn = `<button class="button popup__message-btn">${btnText}</button>`;
    }
    const html = `
        <div class="popup">
            <div class="popup__backdrop"></div>
            <div class="popup__container">
                <div class="popup__close-btn">&times</div>
                <div class="popup__message">
                    <p dir="rtl">${message}</p>
                    ${btn}
                </div>
            </div>
        </div>
    `;
    $popupDiv.insertAdjacentHTML('beforeend', html);
};

export const errorHandler = (message, ) => {
    const loader = $popupDiv.querySelector('.popup:last-child')
    loader.parentNode.removeChild(loader);
    renderMessage(message);
    popupFunctionality();
};

export const renderUser = (userName, profileImg) => {
    document.querySelector('.header__user').innerHTML = '';
    // let newUserName = userName;
    // if (userName.length > 15) {
    //     newUserName = userName.slice(0, 16) + '...';
    // }
    const html = `
        <div class="header__userLinks">
            <a class="header__userLinks--profileImg" href="/profile/${userName}/page/1"><img src="${profileImg}"></a>
            <a class="header__userLinks--userName" href="/profile/${userName}/page/1">${userName}</a>
        </div>
        <a class="btn header__btn header__btn--signout" id="signout-btn">خروج</a>
    `;
    document.querySelector('.header__user').insertAdjacentHTML('afterbegin', html);
    if (!persianText(userName)) {
        document.querySelector('.header__userLinks--userName').classList.add('englishFont-secondary');
    }
};

export const renderCard = (cardObject) => {
    // document.getElementById('photosec').innerHTML = '';

    // for (let i = 0; i < pageObjArr.length; i++) {
        // const cardOwnerData = await axios(`/users/profile/${pageObjArr[i].owner.toString()}`);
        const date = new Date(cardObject.createdAt); 
        const m = moment(date).locale('fa');
        const pDate = `${m.format('D')} ${m.format('MMMM')} ${m.format('YYYY')}`;
        const html = `
            <div class="card">
                <a class="card__figure" href="${cardObject.cardHref}">
                    <img class="card__img" src="${cardObject.sImageSrc}" alt="gallery figure / تصویر گالری">
                </a>
                <div class="card__info">
                    <div class="card__author">
                        <a class="card__author--avatar" href="/profile/${cardObject.ownerName}/page/1">
                            <img class="card__author--img" src="${cardObject.ownerProfileImg}" alt="author avatar / تصویر کاربر">
                        </a>
                        <a dir="auto" class="card__author--name" href="/profile/${cardObject.ownerName}/page/1">${cardObject.ownerName}</a>
                    </div>
                    <p class="card__trivia">
                        <span dir="rtl" class="card__trivia--date">${pDate}</span>
                        <span class="card__trivia--size">${cardObject.imageWidth} &times ${cardObject.imageHeight}</span>
                    </p>
                </div>
            </div>
        `;

        document.querySelector('.gallery').insertAdjacentHTML('beforeend', html);
        if (!persianText(cardObject.ownerName)) {
            const links = document.querySelectorAll(`.card__author--name[href="/profile/${cardObject.ownerName}/page/1"]`);
            const els = Array.prototype.slice.call(links); 
            els.forEach(el => el.classList.add('englishFont-secondary'));
        }
    // }
};

export const renderPaginationNav = (numPages, path, category, searchPhrase, profileName, token) => {
    let markupArr = [];
    
    if (path.startsWith('/page/') || path === "/") {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/page/${i + 1}">${i + 1}</a>`);
        }
    } else if (path.startsWith('/category/')) {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/category/${category}/page/${i + 1}">${i + 1}</a>`);
        }
    } else if (path.startsWith('/search/')) {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/search/${searchPhrase}/page/${i + 1}">${i + 1}</a>`);
        }
    } else if (path.startsWith('/profile/')) {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/profile/${profileName}/page/${i + 1}">${i + 1}</a>`);
        }
    } else if (path.startsWith('/profile-saves/')) {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/profile-saves/${profileName}/page/${i + 1}">${i + 1}</a>`);
        }
    } else if (path.startsWith('/unapproved/')) {
        for (let i = 0; i < numPages; i++) {
            markupArr.push(`<a id="btn_${i + 1}" class="navigation__page" href="/unapproved/${token}/page/${i + 1}">${i + 1}</a>`);
        }
    }
    // document.getElementById('pagination-nav').innerHTML = '';
    markupArr.forEach(cur => {
        document.getElementById('pagination-nav').insertAdjacentHTML('beforeend', cur);
    });
};  

export const updateActivePage = (i) => {
    const container = document.getElementById('pagination-nav');
    const page = document.querySelector('#pagination-nav a');
    const pageStyles = getComputedStyle(page);
    const containerStyles = getComputedStyle(container);
    const btnArr = Array.from(document.querySelectorAll('#pagination-nav a'));
    for (let cur of btnArr) {
        cur.className = 'navigation__page';
    }
    document.getElementById(`btn_${i + 1}`).classList.add('navigation__page--cur');

    if (window.innerWidth < 600) {
        if (container.scrollHeight > parseInt(containerStyles.height.replace('px', ''))) {
            const top = (2 * parseInt(pageStyles.marginTop.replace('px', '')) + parseInt(pageStyles.height.replace('px', ''))) * (i / 3 - 1);
            container.scrollTo(0, top);
        } else {
            container.style.overflow = 'hidden';
            container.style.height = 'unset';
            container.style.transform = 'translateY(-2px)';
        }
    } else {
        if (container.scrollWidth > parseInt(containerStyles.width.replace('px', ''))) {
            const left = (2 * parseInt(pageStyles.marginLeft.replace('px', '')) + parseInt(pageStyles.width.replace('px', ''))) * (i - 3);
            container.scrollTo(left, 0);
        } else {
            container.style.overflow = 'hidden';
            container.style.height = 'unset';
            container.style.transform = 'translateY(-2px)';
        }
    }
};

export const PrevNextBtn = (type) => {
    const curHref = document.querySelector('.navigation__page--cur').getAttribute('href');
    const curPageNum = parseInt(curHref.split('page/')[1]);
    let btnHref;
    if (type === 'backward') {
        btnHref = `${curHref.split('page/')[0]}page/${curPageNum - 1}`;
    } else if (type === 'forward') {
        btnHref = `${curHref.split('page/')[0]}page/${curPageNum + 1}`;
    }
    document.querySelector(`.navigation__${type}`).setAttribute('href', `${btnHref}`);
};

export const search = () => {
    document.querySelector('.search__form').addEventListener('submit', (event) => {
        event.preventDefault();
        const searchPhrase = document.querySelector('.search__input').value;
        if (!searchPhrase) {
            renderMessage('عبارت جستجوی خود را وارد کنید');
            popupFunctionality();
        } else {
            location.href = `/search/${searchPhrase}/page/1`;
        }
    });
};


// export const categoryConvertor = (eCategory) => {
//     const eCategories = ['technology', 'aviation', 'city', 'food', 'painting', 'animals', 'game', 'space', 'cat', 'dog', 'car', 'nature', 'fantasy', 'texture', 'sports', 'interior'];
//     const pCategories = ['تکنولوژی', 'هوانوردی', 'شهر', 'خوراک', 'نقاشی', 'جانوران', 'بازی', 'فضا', 'گربه', 'سگ', 'خودرو', 'چهر', 'فانتزی', 'بافت', 'ورزشی', 'طراحی داخلی'];

//     const pCategory = pCategories[eCategories.indexOf(eCategory)];
//     return pCategory;
// }