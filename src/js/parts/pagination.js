import axios from 'axios';

import { cardFunctionality, renderPaginationNav, renderCard, updateActivePage, PrevNextBtn, renderMessage, popupFunctionality } from './utils';

const cardPerPage = 3;
const path = decodeURIComponent(location.pathname);
let pageNum = parseInt(path.split('page/')[1]);

const homePagination = async (userToken) => {
    if (path.startsWith('/page/') || path === '/') {
        if (path === '/') {
            pageNum = 1;
        }
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get(`/numPages/page/${cardPerPage}/noValue`);
        if (pageNum === res.data.numPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (res.data.numPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
        } else {
            renderPaginationNav(res.data.numPages, path);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/page/card/${pageNum}/${cardPerPage}/${i}`);
                if (i === cardObject.data.numCards) {
                    break;
                }
                const cardOwner = await axios.get(`/users/trivia/${cardObject.data.card.ownerId}`);
                cardObject.data.card.ownerName = cardOwner.data.name;
                cardObject.data.card.ownerProfileImg = cardOwner.data.profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }

    if (path.startsWith('/category/')) {
        const category = path.split('category/')[1].split('/page')[0];
        document.getElementById(`${category}`).classList.add('categories__btn--active');
        document.getElementById(`${category}`).removeAttribute('href');
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get(`/numPages/category/${cardPerPage}/${category}`);
        if (pageNum === res.data.numPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (res.data.numPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
        } else {
            renderPaginationNav(res.data.numPages, path, category);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/category/card/${category}/${pageNum}/${cardPerPage}/${i}`);
                if (i === cardObject.data.numCards) {
                    break;
                }
                const cardOwner = await axios.get(`/users/trivia/${cardObject.data.card.ownerId}`);
                cardObject.data.card.ownerName = cardOwner.data.name;
                cardObject.data.card.ownerProfileImg = cardOwner.data.profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }

    if (path.startsWith('/search/')) {
        const searchPhrase = path.split('search/')[1].split('/page')[0];
        document.querySelector('.search__input').value = searchPhrase;
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get(`/numPages/search/${cardPerPage}/${searchPhrase}`);
        if (pageNum === res.data.numPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (res.data.numPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
            renderMessage('موردی پیدا نشد')
            popupFunctionality();
        } else {
            renderPaginationNav(res.data.numPages, path, undefined, searchPhrase);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/search/card/${searchPhrase}/${pageNum}/${cardPerPage}/${i}`);
                if (i === cardObject.data.numCards) {
                    break;
                }
                const cardOwner = await axios.get(`/users/trivia/${cardObject.data.card.ownerId}`);
                cardObject.data.card.ownerName = cardOwner.data.name;
                cardObject.data.card.ownerProfileImg = cardOwner.data.profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }

    if (path.startsWith('/unapproved/')) {
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get(`/numPages/unapproved/${cardPerPage}/noValue`);
        if (pageNum === res.data.numPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (res.data.numPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
        } else {
            renderPaginationNav(res.data.numPages, path, undefined, undefined, undefined, userToken);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/page/unapprovedCard/${pageNum}/${cardPerPage}/${i}`, {
                    headers: {
                        'Authorization': `Bearer ${userToken}`
                    }
                });
                if (i === cardObject.data.numCards) {
                    break;
                }
                const cardOwner = await axios.get(`/users/trivia/${cardObject.data.card.ownerId}`);
                cardObject.data.card.ownerName = cardOwner.data.name;
                cardObject.data.card.ownerProfileImg = cardOwner.data.profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }
};

const profilePagination = async (userToken, profileId, profileName, profileImg) => {
    if (path.startsWith('/profile/')) {
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get(`/numPages/profile/${cardPerPage}/${profileId}`);
        if (pageNum === res.data.numPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (res.data.numPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
        } else {
            renderPaginationNav(res.data.numPages, path, undefined, undefined, profileName);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/profile/card/${profileId}/${pageNum}/${cardPerPage}/${i}`);
                if (i === cardObject.data.numCards) {
                    break;
                }
                cardObject.data.card.ownerName = profileName;
                cardObject.data.card.ownerProfileImg = profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }

    if (path.startsWith('/profile-saves/') && userToken) {
        if (pageNum === 1) {
            document.querySelector('.navigation__backward').style.display = 'none';
        }
        const res = await axios.get('/users/saves', {
            headers: {
                'Authorization': `Bearer ${userToken}`
            }
        });
        const numberOfPages = Math.ceil(res.data.length / cardPerPage);
        if (pageNum === numberOfPages) {
            document.querySelector('.navigation__forward').style.display = 'none';
        }
        if (numberOfPages === 0) {
            const el = document.querySelector('.navigation');
            el.parentNode.removeChild(el);
        } else {
            renderPaginationNav(numberOfPages, path, undefined, undefined, profileName);
            updateActivePage(pageNum - 1);
            PrevNextBtn('backward');
            PrevNextBtn('forward');
            document.querySelector('.navigation__page--cur').removeAttribute('href');
            for (let i = 0; i < cardPerPage; i++) {
                const cardObject = await axios.get(`/profile-saves/card/${profileId}/${pageNum}/${cardPerPage}/${i}`, {
                    headers: {
                        'Authorization': `Bearer ${userToken}`
                    }
                });
                if (i === cardObject.data.numCards) {
                    break;
                }
                // console.log(cardObject)
                const cardOwner = await axios.get(`/users/trivia/${cardObject.data.card.ownerId}`);
                cardObject.data.card.ownerName = cardOwner.data.name;
                cardObject.data.card.ownerProfileImg = cardOwner.data.profileImg;
                renderCard(cardObject.data.card);
            }
            cardFunctionality();
        }
    }
}

export default {
    homePagination,
    profilePagination
};


