import validator from 'validator';

let passwordValue;

const inputValidity = (type, value) => {
    switch (type) {
        case 'email':
            if (value.trim() === '') {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                };
            }
            if (!validator.isEmail(value.trim())) {
                return {
                    validity: false,
                    message: 'لطفا یک آدرس ایمیل وارد کنید'
                };

            }
            return {
                validity: true
            };
        case 'name': 
            if (value.trim() === '') {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                };
            }
            if (value.trim().length > 20) {
                return {
                    validity: false,
                    message: 'نام کاربری نمی تواند بیشتر از 20 کاراکتر باشد'
                };
            }
            return {
                validity: true
            };
        case 'password-signup':
            if (value.trim() === '') {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                };
            }
            if (value.trim().length < 6) {
                return {
                    validity: false,
                    message: 'کمینه 6 کاراکتر لازم است'
                }
            }
            return {
                validity: true
            };
        case 'password-signup-repeat': 
            if (value !== passwordValue) {
                return {
                    validity: false,
                    message: 'تکرار گذرواژه مطابقت ندارد'
                };
            }
            return {
                validity: true
            };
        case 'password-signin':
            if (value.trim() === '') {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                };
            }
            return {
                validity: true
            };
        case 'profile-image-file':
            if (value && value.type !== 'image/jpeg' && value.type !== 'image/png') {
                return {
                    validity: false,
                    message: 'فرمت فایل اشتباه است'
                };
            }
            if (value && value.size > 2000000) {
                return {
                    validity: false,
                    message: 'حجم فایل باید کمتر از 2 مگابایت باشد'
                };
            }
            return {
                validity: true
            };
        case 'image-file':
            if (value === undefined) {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                };
            }
            if (value.type !== 'image/jpeg') {
                return {
                    validity: false,
                    message: 'فرمت فایل اشتباه است'
                };
            }
            if (value.size > 10000000) {
                return {
                    validity: false,
                    message: 'حجم فایل باید کمتر از 10 مگابایت باشد'
                };
            }
            return {
                validity: true
            };
        case 'select':
            return {
                validity: true
            };
        case 'textarea':
            return {
                validity: true
            };
        case 'cardName':
            if (!value) {
                return {
                    validity: false,
                    message: 'این بخش نمی تواند خالی باشد'
                }
            }
            return {
                validity: true
            };
        case 'approve':
            return {
                validity: true
            };
    }
};

const formValidity = () => {
    const fields = document.querySelectorAll('.form__input');
    const fieldsArr = Array.prototype.slice.call(fields);
    const fieldsState = fieldsArr.every(field => inputValidity(field.name, field.value).validity);
    const fileInput = document.querySelector('.form__file-input input');
    if (fileInput) {
        const fileInputState = inputValidity(fileInput.name, fileInput.files[0]).validity;
        if (fieldsState && fileInputState) {
            return true;
        } else {
            return false;
        }
    } else {
        if (fieldsState) {
            return true;
        } else {
            return false;
        }
    }
};

const checkFormValidity = () => {
    const inputs = document.querySelectorAll('.inputEl');
    const inputsArr = Array.prototype.slice.call(inputs);
    inputsArr.forEach(input => {
        const $submitBtn = document.querySelector('.form__btn');
        const $errMessage = document.querySelector(`.form__errMessage--${input.name}`);
        input.addEventListener('input', () => {
            if (input.name === 'password-signup') {
                passwordValue = input.value;
            }
            const state = inputValidity(input.name, input.value);
            if (state.validity === false) {
                input.classList.add('form__input--red');
                $errMessage.textContent = state.message;
                $submitBtn.setAttribute('disabled', true);
            } else if (state.validity === true) {
                input.classList.remove('form__input--red');
                $errMessage.textContent = '';
                if (formValidity()) {
                    $submitBtn.removeAttribute('disabled');
                }
            }
        });
        if (input.name === 'profile-image-file' || input.name === 'image-file') {
            input.addEventListener('change', () => {
                const state = inputValidity(input.name, input.files[0]);
                const $label = document.querySelector('.form__file-input label');
                if (state.validity === false) {
                    $label.classList.add('red-label');
                    $label.textContent = 'انتخاب کن';
                    $errMessage.textContent = state.message;
                    $submitBtn.setAttribute('disabled', true);
                } else {
                    $label.classList.remove('red-label');
                    if (input.files.length === 0) {
                        $label.textContent = 'انتخاب کن';
                    } else {
                        $label.textContent = 'انتخاب شده';
                    }
                    $errMessage.textContent = '';
                    if (formValidity()) {
                        $submitBtn.removeAttribute('disabled');
                    }
                }
            });
        }
    });
};

export default checkFormValidity;

