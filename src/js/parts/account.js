import axios from 'axios';

import checkFormValidity from './formValidation';
import { renderLoader, popupFunctionality, errorHandler } from './utils';

const $popupDiv = document.getElementById('popup');

const signin = () => {
    document.getElementById('signin-btn').addEventListener('click', () => {
        const html = `
            <div class="popup" id="signin-popup">
                <div class="popup__backdrop"></div>
                <div class="popup__container">
                    <div class="popup__close-btn">&times</div>
                    <form class="form" method="post" id="signin-form">
                        <input class="form__input inputEl" type="email" name="email" id="email-input" autofocus>
                        <label class="form__label" for="email-input">ایمیل</label>
                        <span class="form__errMessage form__errMessage--email"></span>
                        <input class="form__input inputEl" type="password" name="password-signin" id="password-input">
                        <label class="form__label" for="password-input">گذرواژه</label>
                        <span class="form__errMessage form__errMessage--password-signin"></span>
                        <button class="form__btn button" type="submit" disabled>ورود</button>
                    </form>
                </div>
            </div>
        `;
    
        $popupDiv.insertAdjacentHTML('beforeend', html);
        popupFunctionality();
        checkFormValidity();

        document.getElementById('signin-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            renderLoader();

            const data = {
                email: document.getElementById('email-input').value,
                password: document.getElementById('password-input').value
            };
            try {
                const res = await axios.post('/users/signin', data);
                if (res.data.errmsg) {
                    errorHandler(res.data.errmsg);
                } else {
                    localStorage.setItem('imageirToken', res.data.token.toString());
                    localStorage.setItem('imageirName', res.data.user.name);
                    localStorage.setItem('imageirAvatar', res.data.user.profileImg);
                    localStorage.setItem('imageirEmail', res.data.user.email);
                    localStorage.setItem('imageirActivated', res.data.user.activated);
                    localStorage.setItem('imageirAdmin', res.data.user.admin);
                    
                    location.reload();
                }

            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        });
    });
};

const signup = () => {
    document.getElementById('signup-btn').addEventListener('click', () => {
        const html = `
            <div class="popup" id="signup-popup">
                <div class="popup__backdrop"></div>
                <div class="popup__container">
                    <div class="popup__close-btn">&times</div>
                    <form class="form" method="post" id="signup-form">
                        <input class="form__input inputEl" type="text", name="name", id="name-input" autofocus>
                        <label class="form__label" for="name-input">نام کاربری</label>
                        <span class="form__errMessage form__errMessage--name"></span>
                        <input class="form__input inputEl" type="email" name="email" id="email-input">
                        <label class="form__label" for="email-input">ایمیل</label>
                        <span class="form__errMessage form__errMessage--email"></span>
                        <input class="form__input inputEl" type="password" name="password-signup" id="password-input" placeholder="کمینه 6 کاراکتر">
                        <label class="form__label" for="password-input">گذرواژه</label>
                        <span class="form__errMessage form__errMessage--password-signup"></span>
                        <input class="form__input inputEl" type="password" name="password-signup-repeat" id="password-input-repeat" placeholder="کمینه 6 کاراکتر">
                        <label class="form__label" for="password-input-repeat">تکرار گذرواژه</label>
                        <span class="form__errMessage form__errMessage--password-signup-repeat"></span>
                        <div class="form__file-input">
                            <label for="file-input">انتخاب کن</label>
                            <input class="inputEl" type="file" name="profile-image-file" id="file-input" accept="image/png, image/jpeg">
                            <p>(بیشینه 4 مگابایت) <span>(jpg, png)</span> عکس پروفایل</p>
                        </div>
                        <span class="form__errMessage form__errMessage--profile-image-file"></span>
                        <button class="form__btn button" type="submit" disabled>ثبت نام</button>
                    </form>
                </div>
            </div>
        `;
    
        $popupDiv.insertAdjacentHTML('beforeend', html);
        popupFunctionality();
        checkFormValidity();

        document.getElementById('signup-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            renderLoader();

            const data = {
                name: document.getElementById('name-input').value,
                email: document.getElementById('email-input').value,
                password: document.getElementById('password-input').value
            };
            const file = document.getElementById('file-input').files;

            try {
                const res = await axios.post('/users/signup', data);
                if (res.data.errmsg && res.data.keyValue.email) {
                    return errorHandler(`ایمیل به آدرس ${res.data.keyValue.email} قبلا انتخاب شده است &#9888;`);
                }
                if (res.data.errmsg && res.data.keyValue.name) {
                    return errorHandler(`نام کاربری ${res.data.keyValue.name} قبلا انتخاب شده &#9888;`);
                }
                if (file.length > 0) {
                    const formData = new FormData();
                    formData.set('profileImg', file[0]);

                    const avatar = await axios.post(`/users/profileImg`, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': `Bearer ${res.data.token}`
                        }
                    });
                    localStorage.setItem('imageirAvatar', avatar.data);
                } else {
                    localStorage.setItem('imageirAvatar', res.data.user.profileImg);
                }

                localStorage.setItem('imageirToken', res.data.token.toString());
                localStorage.setItem('imageirName', res.data.user.name);
                localStorage.setItem('imageirEmail', res.data.user.email);
                localStorage.setItem('imageirActivated', res.data.user.activated);
                localStorage.setItem('imageirAdmin', res.data.user.admin);
                // send activation email
                location.reload();
            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        });
    });
};

const signout = (token) => {
    document.getElementById('signout-btn').addEventListener('click', async () => {
        renderLoader();
        try {
            await axios.get('/users/signout', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            localStorage.removeItem('imageirToken');
            localStorage.removeItem('imageirName');
            localStorage.removeItem('imageirAvatar');
            localStorage.removeItem('imageirEmail');
            localStorage.removeItem('imageirActivated');
            localStorage.removeItem('imageirAdmin');
            
            location.reload();
        } catch(e) {
            errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
        }
    });
};

export default {
    signin,
    signup,
    signout
};