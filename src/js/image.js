import axios from 'axios';
import jQuery from 'jquery';

import '../sass/pages/image.scss';
import { renderUser, renderMessage, renderLoader, errorHandler, popupFunctionality, search, persianText } from './parts/utils';
import checkFormValidity from './parts/formValidation';
import account from '../js/parts/account';

const profileName = document.querySelector('.actualImage__ownerName');
const category = document.querySelector('.actualImage__btn--category');
const caption = document.querySelector('.actualImage__caption');
const commentInput = document.querySelector('.comment__textarea');
const popupDiv = document.getElementById('popup');
const image = document.querySelector('.actualImage__image');

const token = localStorage.getItem('imageirToken');
const userName = localStorage.getItem('imageirName');
const avatar = localStorage.getItem('imageirAvatar');
const activated = localStorage.getItem('imageirActivated');
const admin = localStorage.getItem('imageirAdmin');

const path = decodeURIComponent(location.pathname);
const cardName = path.replace('/imagePage/', '').split('/')[0];
const cardId = path.replace('/imagePage/', '').split('/')[1];
const approved = image.className.includes('true') ? true : false;

if (!persianText(profileName.textContent)) {
    document.querySelector('.actualImage__ownerName').classList.add('englishFont-secondary');
}

if (caption.textContent.length === 0) {
    caption.parentNode.removeChild(caption);
}

// const autoResizeTextarea = (element) => {
//     let previousScroll = 34;
//     let previousHeight;
//     element.addEventListener('input', () => {
//         console.log('------------------')
//         console.log('previousScroll', previousScroll);
//         if (element.scrollHeight > previousScroll) {
//             const height = parseInt(getComputedStyle(element).height.replace('px', ''));
//             previousHeight = height;
//             element.style.height = `${element.scrollHeight}px`;
//             previousScroll = element.scrollHeight;
//         } else if (element.scrollHeight < previousScroll) {
//             element.style.height = `${previousHeight}px`;
//             previousScroll = element.scrollHeight;
//         }
//         // console.log('height', height)
//         console.log('scroll', element.scrollHeight);
//     })
// };
// autoResizeTextarea(commentInput);

const renderComments = async () => {
    const comments = await axios.get(`/readCardComments/${cardId}`);

    if (comments) {
        for (let i = 0; i < comments.data.length; i++) {
            const commentAuthor = await axios.get(`/users/trivia/${comments.data[i].authorId}`);
            const commentHtml = `
                <li class="comment__item" id="${comments.data[i]._id}">
                    <div class="comment__opinion comment__opinion--comment">
                        <div class="comment__opinionBtns">
                            <a class="comment__delete comment__delete--opinion">
                                <img class="comment__delete--icon" src="/img/bin.png">
                            </a>
                            <button class="comment__replyBtn button">پاسخ</button>
                        </div>
                        <div class="comment__opinionInfo">
                            <p class="comment__opinionText" dir="rtl">${comments.data[i].comment}</p>
                            <a class="comment__commentator" href="/profile/${commentAuthor.data.name}/page/1">
                                <img class="comment__commentator--avatar" src="${commentAuthor.data.profileImg}">
                                <p>${commentAuthor.data.name}</p>
                            </a>
                        </div>
                    </div>
                </li>
            `;
            document.querySelector('.comment__container').insertAdjacentHTML('beforeend', commentHtml);
            const commentEl = document.getElementById(comments.data[i]._id);
            if (!token) {
                commentEl.querySelector('.comment__opinionBtns').style.display = 'none';
            } else if ((commentAuthor.data.name !== userName) && admin === 'false') {
                commentEl.querySelector('.comment__delete--opinion').style.display = 'none';
            }
            if (!persianText(commentEl.querySelector('.comment__commentator p').textContent)) {
                commentEl.querySelector('.comment__commentator p').classList.add('englishFont-secondary');
            }

            for (let n = 0; n < comments.data[i].replies.length; n++) {
                const replyAuthor = await axios.get(`/users/trivia/${comments.data[i].replies[n].authorId}`);
                const replyHtml = `
                    <div class="comment__opinion comment__opinion--reply" id="${comments.data[i].replies[n]._id}">
                        <div class="comment__opinionBtns">
                            <a class="comment__delete comment__delete--reply">
                                <img class="comment__delete--icon" src="/img/bin.png">
                            </a>
                        </div>
                        <div class="comment__opinionInfo">
                            <p dir="rtl" class="comment__opinionText">${comments.data[i].replies[n].reply}</p>
                            <a class="comment__commentator" href="/profile/${replyAuthor.data.name}/page/1">
                                <img class="comment__commentator--avatar" src="${replyAuthor.data.profileImg}">
                                <p>${replyAuthor.data.name}</p>
                            </a>
                        </div>
                    </div>
                `;
                document.getElementById(`${comments.data[i]._id.toString()}`).insertAdjacentHTML('beforeend', replyHtml);
                const replyEl = document.getElementById(comments.data[i].replies[n]._id);
                if (!token) {
                    replyEl.querySelector('.comment__opinionBtns').style.display = 'none';
                } else if ((replyAuthor.data.name !== userName) && admin === 'false') {
                    replyEl.querySelector('.comment__delete--reply').style.display = 'none';
                }
                if (!persianText(replyEl.querySelector('.comment__commentator p').textContent)) {
                    replyEl.querySelector('.comment__commentator p').classList.add('englishFont-secondary');
                }
            }
        }
    }
};
renderComments();

account.signup();
account.signin();

search();

document.querySelector('.actualImage__btn--download').addEventListener('click', async (event) => {
    event.preventDefault();
    await axios.patch('/cards/downloads', { cardID: cardId }, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    const numDownloads = +document.querySelector('.actualImage__btn--download p').textContent;
    document.querySelector('.actualImage__btn--download p').textContent = numDownloads + 1;
    location.href = document.querySelector('.actualImage__btn--download').getAttribute('href');
});

if (token) {
    renderUser(userName, avatar);
    account.signout(token);

    if (activated === 'true') {
        document.querySelector('.actualImage__otherUserBtns').style.display = 'flex';
        document.querySelector('.comment__sender').style.display = 'flex';

        axios.get(`/users/saves/${cardId}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }).then(res => {
            if (res.data.isSaved) {
                document.querySelector('.actualImage__btn--saveIcon').classList.add('actualImage__btn--savedIcon');
            }
        });

        axios.get(`/users/likes/${cardId}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }).then(res => {
            if (res.data.isLiked) {
                document.querySelector('.actualImage__btn--likeIcon').style.fill = 'red';
            }
        })

        document.querySelector('.actualImage__btn--save').addEventListener('click', async () => {
            try {
                const res = await axios.get(`/users/saves/${cardId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                if (!res.data.isSaved) {
                    await axios.post('/users/saves', { cardID: cardId }, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    document.querySelector('.actualImage__btn--saveIcon').classList.add('actualImage__btn--savedIcon');
                } else {
                    await axios.delete(`/users/saves/${cardId}`, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    document.querySelector('.actualImage__btn--saveIcon').classList.remove('actualImage__btn--savedIcon');
                }
            } catch(e) {
                renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                popupFunctionality();
            }   
        });

        document.querySelector('.actualImage__btn--like').addEventListener('click', async () => {
            try {
                const res = await axios.get(`/users/likes/${cardId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                await axios.patch('/cards/likes', { cardID: cardId }, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                if (!res.data.isLiked) {
                    await axios.post('/users/likes', { cardID: cardId }, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    const numLikes = +document.querySelector('.actualImage__btn--like p').textContent;
                    document.querySelector('.actualImage__btn--like p').textContent = numLikes + 1;
                    document.querySelector('.actualImage__btn--likeIcon').style.fill = 'red';
                } else {
                    await axios.delete(`/users/likes/${cardId}`, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    const numLikes = +document.querySelector('.actualImage__btn--like p').textContent;
                    document.querySelector('.actualImage__btn--like p').textContent = numLikes - 1;
                    document.querySelector('.actualImage__btn--likeIcon').style.fill = 'transparent';
                }
            } catch(e) {
                renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                popupFunctionality();
            }
        });

        // COMMENTS
        document.querySelector('.comment__btn').addEventListener('click', async () => {
            const data = {
                comment: document.querySelector('.comment__textarea').value
            };
            if (data.comment.length === 0) {
                renderMessage('لطفا چیزی بنویسید');
                popupFunctionality();
            } else {
                try {
                    const commentID = await axios.post(`/cards/comment/${cardId}`, data, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });
    
                    const newComment = `
                        <li class="comment__item" id="${commentID.data}">
                            <div class="comment__opinion comment__opinion--comment">
                                <div class="comment__opinionBtns">
                                    <a class="comment__delete comment__delete--opinion">
                                        <img class="comment__delete--icon" src="/img/bin.png">
                                    </a>
                                    <button class="comment__replyBtn button">پاسخ</button>
                                </div>
                                <div class="comment__opinionInfo">
                                    <p class="comment__opinionText" dir="rtl">${data.comment}</p>
                                    <a class="comment__commentator" href="/profile/${userName}/page/1">
                                        <img class="comment__commentator--avatar" src="${avatar}">
                                        <p>${userName}</p>
                                    </a>
                                </div>
                            </div>
                        </li>
                    `;

                    document.querySelector('.comment__container').insertAdjacentHTML('beforeend', newComment);
                    if (!persianText(userName)) {
                        document.getElementById(`${commentID.data}`).querySelector('.comment__commentator p').classList.add('englishFont-secondary');
                    }
                    document.querySelector('.comment__textarea').value = '';
                } catch(e) {
                    renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                    popupFunctionality();
                }
            }

        });

        document.querySelector('.comment__container').addEventListener('click', async (event) => {
            if (event.target.className.includes('comment__replyBtn')) {
                const replySender = `
                    <div class="comment__sender comment__sender--reply">
                        <textarea dir="rtl" class="comment__textarea" placeholder="پاسخ شما"></textarea>
                        <div class="comment__replySenderBtns">
                            <button class="comment__btn comment__btn--reply button">فرستادن</button>
                            <button class="comment__btn comment__btn--cancel button">انصراف</button>
                        </div>
                    </div>
                `;
                const commentItem = event.target.parentNode.parentNode;
                event.target.setAttribute('disabled', true);
                commentItem.insertAdjacentHTML('afterend', replySender);
                const replyInputEl = commentItem.parentNode.querySelector('.comment__textarea');
                jQuery(replyInputEl).focus();
                // autoResizeTextarea(replyInputEl);

                commentItem.parentNode.querySelector('.comment__btn--cancel').addEventListener('click', () => {
                    const replySenderEl = commentItem.parentNode.querySelector('.comment__sender--reply');
                    replySenderEl.parentNode.removeChild(replySenderEl);
                    event.target.removeAttribute('disabled');
                });

                commentItem.parentNode.querySelector('.comment__btn--reply').addEventListener('click', async () => {
                    const data = {
                        reply: commentItem.parentNode.querySelector('.comment__textarea').value
                    };
                    if (data.reply.length === 0) {
                        renderMessage('چیزی برای پاسخ بنویسید');
                        popupFunctionality();
                    } else {
                        try {
                            const replyID = await axios.post(`/cards/reply/${cardId}/${commentItem.parentNode.id}`, data, {
                                headers: {
                                    'Authorization': `Bearer ${token}`
                                }
                            });
                            const newReply = `
                                <div class="comment__opinion comment__opinion--reply" id="${replyID.data}">
                                    <div class="comment__opinionBtns">
                                        <a class="comment__delete comment__delete--reply">
                                            <img class="comment__delete--icon" src="/img/bin.png">
                                        </a>
                                    </div>
                                    <div class="comment__opinionInfo">
                                        <p dir="rtl" class="comment__opinionText">${data.reply}</p>
                                        <a class="comment__commentator" href="/profile/${userName}/page/1">
                                            <img class="comment__commentator--avatar" src="${avatar}">
                                            <p>${userName}</p>
                                        </a>
                                    </div>
                                </div>
                            `;
                            const replySenderEl = commentItem.parentNode.querySelector('.comment__sender--reply');
                            replySenderEl.parentNode.removeChild(replySenderEl);
                            commentItem.parentNode.insertAdjacentHTML('beforeend', newReply);
                            if (!persianText(userName)) {
                                document.getElementById(`${replyID.data}`).querySelector('.comment__commentator p').classList.add('englishFont-secondary');
                            }
                            event.target.removeAttribute('disabled');
                        } catch(e) {
                            renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                            popupFunctionality();
                        }
                    }
                });
            }
  
            if (event.target.parentNode.className.includes('comment__delete--reply')) {
                const reply = event.target.parentNode.parentNode.parentNode;
                const replier = reply.querySelector('.comment__commentator p').textContent;
                if (replier === userName || admin === 'true' || profileName.textContent === userName) {
                    try {
                        await axios.delete(`/cards/reply/${cardId}/${reply.parentNode.id}/${reply.id}`, {
                            headers: {
                                'Authorization': `Bearer ${token}`
                            }
                        });

                        reply.parentNode.removeChild(reply);
                    } catch(e) {
                        renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                        popupFunctionality();
                    }
                }
            }

            if (event.target.parentNode.className.includes('comment__delete--opinion')) {
                const comment = event.target.parentNode.parentNode.parentNode.parentNode;
                const commenter = comment.querySelector('.comment__commentator p').textContent;
                if (commenter === userName || admin === 'true' || profileName.textContent === userName) {
                    try {
                        await axios.delete(`/cards/comment/${cardId}/${comment.id}`, {
                            headers: {
                                'Authorization': `Bearer ${token}`
                            }
                        });

                        comment.parentNode.removeChild(comment);
                    } catch(e) {
                        renderMessage('ارتباط شما با مشکل مواجه است &#9888;');
                        popupFunctionality();
                    }
                }
            }
        });
        // COMMENTS
    }
    
    if ((userName === profileName.textContent && activated === 'true') || admin === 'true') {
        document.querySelector('.actualImage__userBtns').style.display = 'flex';

        // const approveEl = document.querySelector('.actualImage__approveIndicator');
        // approveEl.style.display = 'flex';
        // if (approveEl.className.includes('true')) {
        //     approveEl.textContent = 'تایید شده';
        // } else {
        //     approveEl.textContent = 'تایید نشده'
        // }

        document.querySelector('.actualImage__btn--delete').addEventListener('click', async () => {
            renderMessage('از پاک کردن عکس مطمئن هستید؟', 'بله پاکش می کنم');
            popupFunctionality();
            document.querySelector('.popup__message-btn').addEventListener('click', async () => {
                await axios.delete(`/cards/delete/${cardId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                location.href = '/';
            })
        });

        document.querySelector('.actualImage__btn--edit').addEventListener('click', async () => {
            let html;
            if (admin === 'true') {
                html = `
                    <div class="popup">
                        <div class="popup__backdrop"></div>
                        <div class="popup__container">
                            <div class="popup__close-btn">&times</div>
                            <form class="form" id="editCard-form">
                                <select class="form__input form__input--select inputEl" name="select" id="select-input" autofocus>
                                    <option value="تکنولوژی">تکنولوژی</option>
                                    <option value="هوانوردی">هوانوردی</option>
                                    <option value="شهر">شهر</option>
                                    <option value="خوراک">خوراک</option>
                                    <option value="نقاشی">نقاشی</option>
                                    <option value="جانوران">جانوران</option>
                                    <option value="بازی">بازی</option>
                                    <option value="فضا">فضا</option>
                                    <option value="گربه">گربه</option>
                                    <option value="سگ">سگ</option>
                                    <option value="خودرو">خودرو</option>
                                    <option value="طبیعت">طبیعت</option>
                                    <option value="فانتزی">فانتزی</option>
                                    <option value="پس زمینه">پس زمینه</option>
                                    <option value="ورزشی">ورزشی</option>
                                    <option value="طراحی داخلی">طراحی داخلی</option>
                                </select>
                                <label class="form__label" for="select-input">دسته</label>
                                <span class="form__errMessage form__errMessage--select"></span>
                                <textarea class="form__input form__input--textarea inputEl" dir="rtl" name="textarea" cols="50" rows="5" id="textarea-input">${caption.textContent}</textarea>
                                <label class="form__label" for="textarea-input">متن</label>
                                <span class="form__errMessage form__errMessage--textarea"></span>
                                <input class="form__input inputEl" type="text" name="cardName" id="cardName-input" value="${cardName}">
                                <label class="form__label" for="cardName-input">نام عکس</label>
                                <span class="form__errMessage form__errMessage--cardName"></span>
                                <input class="form__input form__input--checkbox inputEl" type="checkbox" name="approve" id="approve-input" ${approved ? 'checked' : ''}>
                                <label class="form__label" for="approve-input">تایید</label>
                                <span class="form__errMessage form__errMessage--approve"></span>
                
                                <button class="form__btn button" type="submit" disabled>اعمال تغییرات</button>
                            </form>
                        </div>
                    </div>
                `; 
            } else {
                html = `
                    <div class="popup">
                        <div class="popup__backdrop"></div>
                        <div class="popup__container">
                            <div class="popup__close-btn">&times</div>
                            <form class="form" id="editCard-form">
                                <textarea class="form__input form__input--textarea inputEl" dir="rtl" name="textarea" cols="50" rows="5" id="textarea-input">${caption.textContent}</textarea>
                                <label class="form__label" for="textarea-input">متن</label>
                                <span class="form__errMessage form__errMessage--textarea"></span>
                
                                <button class="form__btn button" type="submit" disabled>اعمال تغییرات</button>
                            </form>
                        </div>
                    </div>
                `; 
            }

            popupDiv.insertAdjacentHTML('beforeend', html);
            if (admin === 'true') {
                const options = document.querySelectorAll('option');
                const optionsArr = Array.prototype.slice.call(options);
                const selected = optionsArr.find(cur => cur.value === category.textContent);
                selected.setAttribute('selected', true);
            }
            popupFunctionality();
            checkFormValidity();

            document.getElementById('editCard-form').addEventListener('submit', async (event) => {
                event.preventDefault();
                renderLoader();

                let data;
                if (admin === 'true') {
                    data = {
                        catgTitle: document.getElementById('select-input').value,
                        caption: document.getElementById('textarea-input').value,
                        cardName: document.getElementById('cardName-input').value,
                        approved: document.getElementById('approve-input').checked
                    };
                } else {
                    data = {
                        caption: document.getElementById('textarea-input').value
                    };
                }

                try {
                    await axios.patch(`/cards/edit/${cardId}`, data, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    });

                    if (cardName !== data.cardName) {
                        location.href = `/imagePage/${data.cardName}/${cardId}`;
                    } else {
                        location.reload();
                    }
                } catch(e) {
                    errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
                }
            })
        });
    }
}
