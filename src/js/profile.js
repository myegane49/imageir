import jQuery from 'jquery';
import axios from 'axios';

import '../sass/pages/profile.scss';
import { popupFunctionality, renderUser, renderMessage, renderLoader, errorHandler, persianText } from './parts/utils';
import checkFormValidity from './parts/formValidation';
import account from '../js/parts/account';
import pagination from './parts/pagination';

const $userInfo = document.querySelector('.userPanel__userInfo');
const $userLinks = document.querySelector('.userPanel__links');
const $popupDiv = document.getElementById('popup');
const $menuBackDrop = document.querySelector('.userPanel__conceal');

const token = localStorage.getItem('imageirToken');
const path = decodeURIComponent(location.pathname);
let profileName;
if (path.startsWith('/profile/')) {
    profileName = path.split('profile/')[1].split('/page')[0];
} else if (path.startsWith('/profile-saves/')) {
    profileName = path.split('profile-saves/')[1].split('/page')[0];
}
if (!persianText(profileName)) {
    document.querySelector('.userPanel__username').classList.add('englishFont-secondary');
}
const profileId = document.querySelector('.userPanel__userInfo').id;
const profileImg = document.querySelector('.userPanel__image').src;

account.signup();
account.signin();

const userMenuHoverRemove = (type) => {
    if (type === 'closeMenu') {
        jQuery($userInfo).hover(() => {
            jQuery($userInfo).css('animation', 'none');
        }, () => {});
    }
    if (type === 'signout') {
        jQuery($userInfo).hover(() => {
            jQuery($userInfo).css('animation', 'none');
            jQuery($userInfo).css('cursor', 'auto');
        }, () => {});
    }
};

const userMenuHover = () => {
    jQuery($userInfo).hover(() => {
        jQuery($userInfo).css('animation', 'userMenuJiggle .2s');
        jQuery($userInfo).css('cursor', 'pointer');
    }, () => {
        jQuery($userInfo).css('animation', 'none');
        jQuery($userInfo).css('cursor', 'auto');
    });
};

const userMenuHandler = () => {
    if ($userInfo.style.right !== '0px') {
        $userInfo.style.right = '0';
        $userInfo.style.transform = 'translateX(0)';
        document.querySelector('.userPanel__conceal').style.width = '0';
        $userLinks.style.left = '0';
        userMenuHoverRemove('closeMenu');
    } else {
        $userInfo.style.right = '50%';
        $userInfo.style.transform = 'translateX(50%)';
        document.querySelector('.userPanel__conceal').style.width = '50%';
        $userLinks.style.left = '50%';
        userMenuHover();
    }
}

const phoneUserMenuHandler = () => {
    $userLinks.style.left = 0;
    $menuBackDrop.style.display = 'block';

    $menuBackDrop.addEventListener('click', () => {
        $userLinks.style.left = '-100%';
        $menuBackDrop.style.display = 'none';
    });
};

const userMenuFunctionality = () => {
    if (window.innerWidth <= 600) {
        $userInfo.addEventListener('click', phoneUserMenuHandler);
        
        // $userLinks.setAttribute('draggable', 'true');
        // $userLinks.addEventListener('dragover', (event) => {
            //     console.log(event);
            // });
    } else {
        const userLinksStyles = getComputedStyle($userLinks);
        const userInfoStyles = getComputedStyle($userInfo);
        document.querySelector('.userPanel').style.width = `${parseInt(userInfoStyles.width) + parseInt(userLinksStyles.width) - 40}px`;
        $userInfo.addEventListener('click', userMenuHandler);
    }
};

const deleteUser = () => {
    document.querySelector('.userPanel__btn--delete').addEventListener('click', () => {
        renderMessage('مطمئنی؟', 'بله حسابم را پاک می کنم');
        popupFunctionality();

        document.querySelector('.popup__message-btn').addEventListener('click', async () => {
            try {
                await axios.delete(`/userCards/removeAllUserCards/${profileId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                await axios.delete(`/users/${profileId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                
                localStorage.removeItem('imageirToken');
                localStorage.removeItem('imageirName');
                localStorage.removeItem('imageirAvatar');
                localStorage.removeItem('imageirEmail');
                localStorage.removeItem('imageirActivated');
                localStorage.removeItem('imageirAdmin');
                location.href = '/';
            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        });
    });
};

const updateUser = () => {
    document.querySelector('.userPanel__btn--edit').addEventListener('click', async () => {
        const profile = await axios.get(`/users/${profileId}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
        const html = `
            <div class="popup">
                <div class="popup__backdrop"></div>
                <div class="popup__container">
                    <div class="popup__close-btn">&times</div>
                    <form class="form" id="editUser-form">
                        <input class="form__input inputEl" type="text", name="name", id="name-input" autofocus value="${profile.data.name}">
                        <label class="form__label" for="name-input">نام کاربری</label>
                        <span class="form__errMessage form__errMessage--name"></span>
                        <input class="form__input inputEl" type="email" name="email" id="email-input" value="${profile.data.email}">
                        <label class="form__label" for="email-input">ایمیل</label>
                        <span class="form__errMessage form__errMessage--email"></span>
                        <div class="form__file-input">
                            <label for="file-input">انتخاب کن</label>
                            <input class="inputEl" type="file" name="profile-image-file" id="file-input" accept="image/png, image/jpeg">
                            <p>(بیشینه 4 مگابایت) <span>(jpg, png)</span> عکس پروفایل</p>
                        </div>
                        <span class="form__errMessage form__errMessage--profile-image-file"></span>
                        <button class="form__btn button" type="submit" disabled>اعمال تغییرات</button>
                    </form>
                </div>
            </div>
        `;
        
        $popupDiv.insertAdjacentHTML('beforeend', html);
        popupFunctionality();
        checkFormValidity();

        document.getElementById('editUser-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            renderLoader();

            const data = {
                name: document.getElementById('name-input').value,
                email: document.getElementById('email-input').value,
            };
            const file = document.getElementById('file-input').files;

            try {
                const res = await axios.patch(`/users/${profileId}`, data, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                if (res.data.errmsg && res.data.keyValue.email) {
                    return errorHandler(`ایمیل به آدرس ${res.data.keyValue.email} قبلا انتخاب شده است &#9888;`);
                }
                if (res.data.errmsg && res.data.keyValue.name) {
                    return errorHandler(`نام کاربری ${res.data.keyValue.name} قبلا انتخاب شده &#9888;`);
                }
                if (file.length > 0) {
                    const formData = new FormData();
                    formData.set('profileImg', file[0]);
                    const avatar = await axios.post(`/users/profileImg`, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    localStorage.setItem('imageirAvatar', avatar.data);
                }
                
                localStorage.setItem('imageirEmail', data.email);
                localStorage.setItem('imageirName', data.name);

                if (profile.data.email !== data.email) {
                    localStorage.setItem('imageirActivated', false);
                }

                if (profileName !== data.name) {
                    const oldAvatar = localStorage.getItem('imageirAvatar');
                    if (oldAvatar !== '/uploads/profileImgs/default-profile-image') {
                        localStorage.setItem('imageirAvatar', `/uploads/profileImgs/${data.name}.jpg`)
                    }
                    location.href = `/profile/${data.name}/page/1`;
                } else {
                    location.reload();
                }
            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        })
    });
};

const updatePassword = () => {
    document.querySelector('.userPanel__btn--editPassword').addEventListener('click', () => {
        const html = `
            <div class="popup">
                <div class="popup__backdrop"></div>
                <div class="popup__container">
                    <div class="popup__close-btn">&times</div>
                    <form class="form" id="editPassword-form">
                        <input class="form__input inputEl" type="password" name="password-signup" id="password-input" placeholder="کمینه 6 کاراکتر">
                        <label class="form__label" for="password-input">گذرواژه</label>
                        <span class="form__errMessage form__errMessage--password-signup"></span>
                        <input class="form__input inputEl" type="password" name="password-signup-repeat" id="password-input-repeat" placeholder="کمینه 6 کاراکتر">
                        <label class="form__label" for="password-input-repeat">تکرار گذرواژه</label>
                        <span class="form__errMessage form__errMessage--password-signup-repeat"></span>
                        <button class="form__btn button" type="submit" disabled>اعمال تغییرات</button>
                    </form>
                </div>
            </div>
        `;
        
        $popupDiv.insertAdjacentHTML('beforeend', html);
        popupFunctionality();
        checkFormValidity();

        document.getElementById('editPassword-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            renderLoader();

            const data = { password: document.getElementById('password-input').value };

            try {
                await axios.patch(`/users/password/${profileId}`, data, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                $popupDiv.innerHTML = '';
                renderMessage('گذرواژه شما تغییر کرد');
                popupFunctionality();
            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        })
    });
};

const addPost = () => {
    document.querySelector('.userPanel__btn--add').addEventListener('click', () => {
        const html = `
            <div class="popup">
                <div class="popup__backdrop"></div>
                <div class="popup__container">
                    <div class="popup__close-btn">&times</div>
                    <form class="form" id="addPost-form">
                        <div class="form__file-input form__file-input--image">
                            <label for="file-input">انتخاب کن</label>
                            <input class="inputEl" type="file" name="image-file" id="file-input" accept="image/jpeg">
                            <p>(بیشینه 10 مگابایت) <span>(jpg)</span> عکس</p>
                        </div>
                        <span class="form__errMessage form__errMessage--image-file"></span>
                        <textarea class="form__input form__input--textarea inputEl" dir="rtl" name="textarea" cols="50" rows="5" id="textarea-input"></textarea>
                        <label class="form__label" for="textarea-input">متن</label>
                        <span class="form__errMessage form__errMessage--textarea"></span>
                        <button class="form__btn button" type="submit" disabled>فرستادن</button>
                    </form>
                </div>
            </div>
        `;

        
        $popupDiv.insertAdjacentHTML('beforeend', html);
        popupFunctionality();
        checkFormValidity();

        document.getElementById('addPost-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            renderLoader();

            const data = {
                caption: document.getElementById('textarea-input').value
            };
            const file = document.getElementById('file-input').files;

            try {
                const card = await axios.post('/cards', data, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                
                if (file.length > 0) {
                    const formData = new FormData();
                    formData.set('image', file[0]);
                    await axios.post(`/cards/upload/${card.data._id.toString()}`, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    $popupDiv.innerHTML = '';
                    renderMessage('پس از تایید مدیریت پست شما روی سایت قرار خواهد گرفت');
                    popupFunctionality();
                }
            } catch(e) {
                errorHandler('ارتباط شما با مشکل مواجه است &#9888;');
            }
        });
    });
};

if (token) {
    const userName = localStorage.getItem('imageirName');
    const avatar = localStorage.getItem('imageirAvatar');
    const activated = localStorage.getItem('imageirActivated');
    const admin = localStorage.getItem('imageirAdmin');

    renderUser(userName, avatar);
    account.signout(token);

    if (activated === 'false' && userName === profileName) {
        renderMessage('برای استفاده از قابلیت های کاربری ایمیل خود را تایید کنید', 'ارسال دوباره ایمیل تایید');
        document.querySelector('.popup__message-btn').addEventListener('click', () => {
            // send activation email
            // render loader
            // empty popup
        });
        popupFunctionality();
    }

    if (admin === 'true' && profileName === userName) {
        const html = `
            <a href="/unapproved/${token}/page/1" class="userPanel__btn userPanel__btn--unapproved">تایید نشده ها</a>
            <div class="userPanel__btn--seperator"></div>
        `;
        document.querySelector('.userPanel__btn--posts').insertAdjacentHTML('beforebegin', html);
    }

    if ((userName === profileName && activated === 'true') || admin === 'true' ) {
        userMenuFunctionality();
        userMenuHover();

        deleteUser();
        updateUser();
        updatePassword();
    }
    
    if (userName === profileName && activated === 'true') {
        addPost();
    }

    pagination.profilePagination(token, profileId, profileName, profileImg);

} else {
    pagination.profilePagination(undefined, profileId, profileName, profileImg);
}

