import '../sass/pages/home.scss';
import { renderUser, search } from './parts/utils';
import account from '../js/parts/account';
import pagination from './parts/pagination';

const token = localStorage.getItem('imageirToken');

account.signup();
account.signin();

search();

if (token) {
    const userName = localStorage.getItem('imageirName');
    const avatar = localStorage.getItem('imageirAvatar');

    renderUser(userName, avatar);
    account.signout(token);
} 

pagination.homePagination(token);

// console.log(window.innerWidth)
// console.log(window.navigator.appCodeName)
// console.log(window.navigator.platform)
// navigator.geolocation.getCurrentPosition((position) => {
//     console.log(position);
// })

