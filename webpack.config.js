const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

let config = {
    entry: {
        home: ['@babel/polyfill', './src/js/home.js'],
        profile: ['@babel/polyfill', './src/js/profile.js'],
        image: ['@babel/polyfill', './src/js/image.js']
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/public/js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /src-back/],
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                autoprefixer({
                                    browsers: [
                                        "> 1%",
                                        "last 3 versions"
                                    ]
                                })
                            ]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('node-sass')
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '/../../src/css/[name].css'
        })
    ]
}

module.exports = (env, argv) => {
    if (argv.mode === 'development') {
        config.module.rules[1].use[0] = 'style-loader'
    }

    return config;
}
