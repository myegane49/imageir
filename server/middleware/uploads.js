const multer = require('multer');

// const avatarStorage = multer.diskStorage({
//     destination: function(req, file, cb) {
//         cb(null, './public/uploads/profileImgs');
//     },
//     filename: function(req, file, cb) {
//         cb(null, `${req.user.name}.jpg`);
//     }
// });

const avatarUpload = multer({
    // storage: avatarStorage,
    limits: {
        fileSize: 2000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('file must be jpg, jpeg or png'));
        }
        cb(undefined, true); 
    }
});

const imageUpload = multer({
    limits: {
        fileSize: 10000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg)$/)) {
            return cb(new Error('file must be jpg or jpeg'));
        }
        cb(undefined, true);
    }
});

module.exports = {
    avatarUpload,
    imageUpload
};

