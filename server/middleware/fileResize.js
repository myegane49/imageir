const sharp = require('sharp');
// const fs = require('fs');

const avatarResize = async (req, res, next) => {
    // const buffer = fs.readFileSync(`./public/uploads/profileImgs/${req.user.name}.jpg`);
    const result = await sharp(req.file.buffer).resize({ width: 200, height: 200 }).jpeg().toFile(`./public/uploads/profileImgs/${req.user.name}.jpg`);
    if (result) {
        req.user.profileImg = `/uploads/profileImgs/${req.user.name}.jpg`;
    }
    next();
}

const imageResize = async (req, res, next) => {
    const smallImageName = `./public/uploads/images/${req.file.originalname.replace('.jpg', '')}_${req.params.id}_small.jpg`;
    const largeImageName = `./public/uploads/images/${req.file.originalname.replace('.jpg', '')}_${req.params.id}_large.jpg`;

    await sharp(req.file.buffer).resize({ width: 596, height: 380 }).toFile(smallImageName);
    await sharp(req.file.buffer).toFile(largeImageName);

    req.body.sImageSrc = smallImageName.replace('./public', '');
    req.body.lImageSrc = largeImageName.replace('./public', '');

    await sharp(req.file.buffer).metadata().then(metadata => {
        req.body.imageSize = metadata.width * metadata.height;
        req.body.imageWidth = metadata.width;
        req.body.imageHeight = metadata.height;
    });

    next();
};

module.exports = {
    avatarResize,
    imageResize
};