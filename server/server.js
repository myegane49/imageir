const path = require('path');
const express = require('express');
const hbs = require('hbs');
const mongoose = require('mongoose');

const cardRouter = require('./routers/card-routes');
const userRouter = require('./routers/user-routes');

const port = process.env.PORT;
const app = express();

const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, './templates/views');
const partialsPath = path.join(__dirname, './templates/partials');

mongoose.connect(process.env.MONGODB_URL, { 
    useNewUrlParser: true,
    useCreateIndex: true 
});

app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

app.use(express.static(publicDirectoryPath));
app.use(express.json());
app.use(userRouter);
app.use(cardRouter);

app.listen(port, () => {
    console.log(`server is up on port ${port}`);
});

