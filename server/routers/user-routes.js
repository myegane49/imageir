const fs = require('fs');

const express = require('express');

const User = require('../models/user');
// const Card = require('../models/card');
const auth = require('../middleware/auth');
const { avatarUpload } = require('../middleware/uploads');
const { avatarResize } = require('../middleware/fileResize');

const router = new express.Router();

router.post('/users/signup', async (req, res) => {
    // try to find a user with the email and by the name
    // if there is already a user with the email return res.send('email in use') and the same for name
    // add the status code for to the catch for when server is down
    const user = new User(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({ user, token });
    } catch(e) {
        res.send(e);
    }
});

router.post('/users/profileImg', auth, avatarUpload.single('profileImg'), avatarResize, async (req, res) => {
    await req.user.save();
    res.send(req.user.profileImg);
}, (error, req, res, next) => {
    res.status(400).send(e);
});

// router.delete('/users/profileImg/remove', auth, async (req, res) => {
//     try {
//         req.user.profileImg = '/img/default-profile-image.jpg';
//         await req.user.save();
//         res.send();
//     } catch(e) {
//         res.status(400).send(e);
//     }
// });

// router.get('/users/:id/profileImg', async (req, res) => {
//     try {
//         const user = await User.findById(req.params.id);
//         if (!user || !user.profileImg) {
//             throw new Error();
//         }
//         res.set('Content-Type', 'image/png');
//         res.send(user.profileImg);
//     } catch(e) {
//         res.status(404).send();
//     }
// });

router.post('/users/signin', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        if (!user) {
            return res.send({errmsg: 'اطلاعات کاربری اشتباه است &#9888;'});
        }
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch(e) {
        res.status(400).send(e);
    }
});

router.get('/users/signout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter(token => token.token !== req.token);
        await req.user.save();
        res.send();
    } catch(e) {
        res.status(500).send(e);
    }
});

// router.post('/users/signoutAll', auth, async (req, res) => {
//     try {
//         req.user.tokens = [];
//         await req.user.save();
//         res.status(200).send();
//     } catch(e) {
//         res.status(500).send();
//     }
// });

// router.get('/users/me', auth, async (req, res) => {
//     res.send(req.user);
// });



// router.get('/users/profile/:id', async (req, res) => {
//     try {
//         const user = await User.findById(req.params.id);
//         res.send({
//             profileName: user.name,
//             profileHref: `/profile/${user.name}`   
//         });
//     } catch(e) {
//         res.status(404).send(e);
//     }
// });

router.get('/profile/:userName/page/:pageNum', async (req, res) => {
    try {
        const user = await User.findOne({ name: req.params.userName });
        if (!user) {
            return res.status(404).send();
        }
        res.render('profile', {
            userName: user.name,
            avatarSrc: user.profileImg,
            userId: user._id,
            title: `${req.params.userName}`
        });
    } catch(e) {
        res.status(400).send();
    }
});

router.delete('/users/:userId', auth, async (req, res) => {
    try {
        if (req.user._id.toString() === req.params.userId || req.user.admin === true) {
            const user = await User.findById(req.params.userId);
            if (user.profileImg === `/uploads/profileImgs/${user.name}.jpg`) {
                fs.unlinkSync(`./public/uploads/profileImgs/${user.name}.jpg`);
            }
            await user.remove();
            res.send();
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.post('/users/saves', auth, async (req, res) => {
    try {
        if (req.user.activated === true) {
            const card = req.user.saves.find(save => save.cardID === req.body.cardID);
            if (!card) {
                req.user.saves = req.user.saves.concat(req.body);
                await req.user.save();
                res.send();
            }
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.get('/users/saves/:cardId', auth, async (req, res) => {
    const cardId = req.params.cardId;

    try {
        if (req.user.activated) {
            const card = req.user.saves.find(save => save.cardID === cardId);
            if (card) {
                res.send({ isSaved: true });
            } else {
                res.send({ isSaved: false });
            }
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.get('/users/saves', auth, async (req, res) => {
    try {
        if (req.user.activated) {
            res.send(req.user.saves);
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.delete('/users/saves/:cardId', auth, async (req, res) => {
    try {
        req.user.saves = req.user.saves.filter(save => save.cardID !== req.params.cardId);
        await req.user.save();
        res.send();
    } catch(e) {
        res.status(500).send(e);
    }
});

router.get('/users/:userId', auth, async (req, res) => {
    const userId = req.params.userId
    try {
        const user = await User.findById(userId);
        if (req.user._id.toString() === userId || req.user.admin === true) {
            res.send(user);
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.get('/users/trivia/:userId', async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        res.send({
            name: user.name,
            profileImg: user.profileImg
        });
        
    } catch(e) {
        res.status(500).send(e);
    }
});

router.patch('/users/password/:userId', auth, async (req, res) => {
    const userId = req.params.userId;
    try {
        if ((userId === req.user._id.toString() && req.user.activated === true) || req.user.admin === true) {
            const user = await User.findById(userId);
            user.password = req.body.password;
            await user.save();
            res.send(req.body);
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.patch('/users/:userId', auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const userId = req.params.userId
    try {
        if ((userId === req.user._id.toString() && req.user.activated === true) || req.user.admin === true) {
            const user = await User.findById(userId);
            if (user.email !== req.body.email) {
                user.activated = false;
            }
            if (user.name !== req.body.name && user.profileImg !== '/uploads/profileImgs/default-profile-image.jpg') {
                user.profileImg = `/uploads/profileImgs/${req.body.name}.jpg`;
                fs.renameSync(`./public/uploads/profileImgs/${user.name}.jpg`, `./public${user.profileImg}`);
                // change all user cards ownername and profileimg
                // const userCards = await Card.find({ ownerName: user.name });
                // userCards.forEach(async (card) => {
                //     card.ownerName = req.body.name;
                //     card.ownerProfileImg = user.profileImg;
                //     await card.save();
                // });
            }
            updates.forEach(update => {
                user[update] = req.body[update];
            });
            await user.save();
            res.send();
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.get('/profile-saves/:userName/page/:pageNum', async (req, res) => {
    try {
        const user = await User.findOne({ name: req.params.userName });
        if (!user) {
            return res.status(404).send();
        } 
        res.render('profile', {
            userName: user.name,
            avatarSrc: user.profileImg,
            userId: user._id,
            title: `${req.params.userName}`
        });
        
    } catch(e) {
        res.status(400).send(e);
    }
});

router.post('/users/likes', auth, async (req, res) => {    
    try {
        if (req.user.activated) {
            const likedByUser = req.user.likes.find(like => like.cardID === req.body.cardID);
            if (!likedByUser) {
                req.user.likes = req.user.likes.concat(req.body);
                await req.user.save();
                res.send();
            }
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.get('/users/likes/:cardId', auth, async (req, res) => {
    const cardId = req.params.cardId;

    try {
        if (req.user.activated === true) {
            const card = req.user.likes.find(like => like.cardID === cardId);
            if (card) {
                res.send({ isLiked: true });
            } else {
                res.send({ isLiked: false });
            }
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.delete('/users/likes/:cardId', auth, async (req, res) => {
    try {
        req.user.likes = req.user.likes.filter(like => like.cardID !== req.params.cardId);
        await req.user.save();
        res.send();
    } catch(e) {
        res.status(500).send(e);
    }
});

module.exports = router;
