const fs = require('fs');

const moment = require('jalali-moment');
const express = require('express');
const mongodb = require('mongodb');
const jwt = require('jsonwebtoken');

const Card = require('../models/card');
const User = require('../models/user');
// const DB = require('../db/mongodb');
const auth = require('../middleware/auth');
const { imageUpload } = require('../middleware/uploads');
const { imageResize } = require('../middleware/fileResize');

const router = new express.Router();
const ObjectID = mongodb.ObjectID;

// const cardPerPage = 3;

router.get('/', (req, res) => {
    res.render('home', {
        title: 'دانلود عکس'
    });
});

router.post('/cards', auth, async (req, res) => {
    // const m = moment().locale('fa');
    // const pDate = `${m.format('D')} ${m.format('MMMM')} ${m.format('YYYY')}`;
    if (req.user.activated) {
        const card = new Card({
            ...req.body,
            ownerId: req.user._id
            
            // pDate
        });
        try {
            await card.save();
            res.status(201).send(card);
        } catch(e) {
            res.status(400).send(e);
        }
    }
});

router.post('/cards/upload/:id', auth, imageUpload.single('image'), imageResize, async (req, res) => {
    if (req.user.activated) {
        const card = await Card.findOne({ _id: req.params.id, ownerId: req.user._id });
        card.sImageSrc = req.body.sImageSrc;
        card.lImageSrc = req.body.lImageSrc;
        card.imageSize = req.body.imageSize;
        card.imageWidth = req.body.imageWidth;
        card.imageHeight = req.body.imageHeight;
        card.cardName = req.file.originalname.replace('.jpg', '');
        card.cardHref = `/imagePage/${card.cardName}/${card._id}`;
        await card.save();
        res.send(); 
    }
}, (error, req, res, next) => {
    res.status(400).send(error);
});

router.get('/numPages/:type/:cardPerPage/:value', async (req, res) => {
    const cardPerPage = parseInt(req.params.cardPerPage);
    const type = req.params.type;
    const value = req.params.value;
    const regex = new RegExp(value);
    try {
        let count, numPages;
        switch (type) {
            case 'page':
                count = await Card.countDocuments({ approved: true });
                numPages = Math.ceil(count / cardPerPage);
                res.send({ numPages });
            case 'category':
                count = await Card.countDocuments({ approved: true, catgTitle: value });
                numPages = Math.ceil(count / cardPerPage);
                res.send({ numPages });
            case 'search':
                count = await Card.countDocuments({ approved: true, cardName: {$regex: regex} });
                numPages = Math.ceil(count / cardPerPage);
                res.send({ numPages });
            case 'profile':
                count = await Card.countDocuments({ approved: true, ownerId: value });
                numPages = Math.ceil(count / cardPerPage);
                res.send({ numPages });
            case 'unapproved':
                count = await Card.countDocuments({ approved: false });
                numPages = Math.ceil(count / cardPerPage);
                res.send({ numPages });
        }
    } catch(e) {
        res.status(500).send();
    }
});

router.get('/page/card/:pageNum/:cardPerPage/:nthCard', async (req, res) => {
    const cardPerPage = parseInt(req.params.cardPerPage);
    const cardNum = parseInt(req.params.nthCard);
    const pageNum = parseInt(req.params.pageNum);
    try {
        const cards = await Card.find({ approved: true }).sort([['createdAt', 'descending']])
            .limit(cardPerPage)
            .skip(pageNum * cardPerPage - cardPerPage);
        const card = cards[cardNum];
        res.send({ card, numCards: cards.length});
    } catch(e) {
        res.status(500).send();
    }
});

router.get('/page/:num', (req, res) => {
    res.render('home', {
        title: 'دانلود عکس'
    });
});

router.get('/category/:cat/page/:num', (req, res) => {
    res.render('home', {
        title: req.params.cat
    });
});

router.get('/category/card/:cat/:pageNum/:cardPerPage/:nthCard', async (req, res) => {
    const pageNum = parseInt(req.params.pageNum);
    const cardPerPage = parseInt(req.params.cardPerPage);
    const category = req.params.cat;
    const cardNum = parseInt(req.params.nthCard)
    try {
        const cards = await Card.find({ approved: true, catgTitle: category }).sort([['createdAt', 'descending']])
            .limit(cardPerPage)
            .skip(pageNum * cardPerPage - cardPerPage);
        const card = cards[cardNum];
        res.send({ card, numCards: cards.length });
    } catch(e) {
        res.status(500).send();
    }
});

router.get('/search/:searchPhrase/page/:pageNum', async (req, res) => {
    res.render('home', {
        title: 'دانلود عکس'
    });
});

router.get('/search/card/:searchPhrase/:pageNum/:cardPerPage/:nthCard', async (req, res) => {
    const pageNum = parseInt(req.params.pageNum);
    const cardNum = parseInt(req.params.nthCard); 
    const regex = new RegExp(req.params.searchPhrase);
    const cardPerPage = parseInt(req.params.cardPerPage);
    try {
        const cards = await Card.find({ approved: true, cardName: {$regex: regex} }).sort([['createdAt', 'descending']])
            .limit(cardPerPage)
            .skip(pageNum * cardPerPage - cardPerPage);
        const card = cards[cardNum];
        res.send({ card, numCards: cards.length });
    } catch(e) {
        res.status(500).send();
    }
});

router.get('/profile/card/:userId/:pageNum/:cardPerPage/:nthCard', async (req, res) => {
    const pageNum = parseInt(req.params.pageNum);
    const cardNum = parseInt(req.params.nthCard);
    const cardPerPage = parseInt(req.params.cardPerPage);
    const userId = req.params.userId;

    try {
        const cards = await Card.find({ approved: true, ownerId: userId }).sort([['createdAt', 'descending']])
            .limit(cardPerPage)
            .skip(pageNum * cardPerPage - cardPerPage);
        const card = cards[cardNum];
        res.send({ card, numCards: cards.length }); 
    } catch(e) {
        res.status(500).send();
    }
});

router.get('/imagePage/:cardName/:cardId', async (req, res) => {
    try {
        const card = await Card.findById(req.params.cardId);
        const owner = await User.findById(card.ownerId);
        if (!card) {
            throw new Error();
        }
        const date = new Date(card.createdAt); 
        const m = moment(date).locale('fa');
        const pDate = `${m.format('D')} ${m.format('MMMM')} ${m.format('YYYY')}`;

        res.render('image', {
            title: `${req.params.cardName}`,
            lImageSrc: card.lImageSrc,
            category: card.catgTitle,
            ownerName: owner.name,
            ownerProfileImg: owner.profileImg,
            pDate,
            imageWidth: card.imageWidth,
            imageHeight: card.imageHeight,
            likeNum: card.likes,
            downloadNum: card.downloads,
            // displayBtn: 'display: none;',
            approved: card.approved,
            caption: card.caption
        });
    } catch(e) {
        res.status(404).send();
    }
});

router.delete('/userCards/removeAllUserCards/:userId', auth, async (req, res) => {
    const userId = req.params.userId;
    try {
        if (req.user._id.toString() === userId || req.user.admin === true) {
            const cards = await Card.find({ ownerId: userId });
            if (cards.length !== 0) {
                cards.forEach(card => {
                    fs.unlinkSync(`./public/${card.sImageSrc}`);
                    fs.unlinkSync(`./public/${card.lImageSrc}`);
                })
                await Card.deleteMany({ ownerId: userId });
            }
            res.send();
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.get('/profile-saves/card/:userId/:pageNum/:cardPerPage/:nthCard', auth, async (req, res) => {
    const pageNum = parseInt(req.params.pageNum);
    const cardNum = parseInt(req.params.nthCard);
    const cardPerPage = parseInt(req.params.cardPerPage);
    const userId = req.params.userId;

    try {
        if (req.user._id.toString() === userId && req.user.activated) {
            const saves = req.user.saves.map(save => save.cardID);
            const cardId = saves[(pageNum - 1) * cardPerPage + cardNum];
            const card = await Card.findById(cardId);

            let numCards;
            if (saves.length >= (cardPerPage * pageNum)) {
                numCards = cardPerPage;
            } else {
                numCards = saves.length % cardPerPage;
            }
            res.send({ card, numCards });
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.patch('/cards/likes', auth, async (req, res) => {
    const cardId = req.body.cardID;

    try {
        if (req.user.activated) {
            const card = await Card.findById(cardId);
            const likedByUser = req.user.likes.find(like => like.cardID === cardId);
            if (!likedByUser) {
                card.likes++;
                await card.save();
                res.send();
            } else if (card.likes >= 0) {
                card.likes--;
                await card.save();
                res.send();
            }
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.patch('/cards/downloads', auth, async (req, res) => {
    const cardId = req.body.cardID;
    try {
        const card = await Card.findById(cardId);
        card.downloads++;
        card.save();
        res.send();       
    } catch(e) {
        res.status(400).send(e);
    }
});

router.delete('/cards/delete/:id', auth, async (req, res) => {
    const cardId = req.params.id;
    try {
        const card = await Card.findById(cardId);        
        if ((req.user._id.toString() === card.ownerId.toString() && req.user.activated) || req.user.admin) {
            const users = await User.find({ saves: { $elemMatch: { cardID: cardId } } });
            if (users.length !== 0) {
                users.forEach(async (user) => {
                    user.saves = user.saves.filter(save => save.cardID !== cardId);
                    await user.save();
                });
            }

            // const users = await User.find({  });

            // const saviors = users.filter(user => !user.saves.every(save => save.cardID !== cardId));
            // saviors.forEach(async (user) => {
            //     user.saves = user.saves.filter(save => save.cardID !== cardId);
            //     await user.save();
            // });

            // const likers = users.filter(user => !user.likes.every(like => like.cardID !== cardId));
            // likers.forEach(async (user) => {
            //     user.likes = user.likes.filter(like => like.cardID !== cardId);
            //     await user.save();
            // });

            await card.remove();
            fs.unlinkSync(`./public/${card.sImageSrc}`);
            fs.unlinkSync(`./public/${card.lImageSrc}`);
            res.send();
        }
    } catch(e) {
        res.status(500).send(e);
    }
});

router.patch('/cards/edit/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body);
    try {
        const card = await Card.findById(req.params.id);
        if ((req.user._id.toString() === card.ownerId.toString() && req.user.activated) || req.user.admin === true) {
            if (req.body.cardName && card.cardName !== req.body.cardName) {
                card.cardHref = `/imagePage/${req.body.cardName}/${card._id}`;
                card.lImageSrc = `/uploads/images/${req.body.cardName}_${card._id}_large.jpg`;
                card.sImageSrc = `/uploads/images/${req.body.cardName}_${card._id}_small.jpg`;
                fs.renameSync(`./public/uploads/images/${card.cardName}_${card._id}_large.jpg`, `./public${card.lImageSrc}`);
                fs.renameSync(`./public/uploads/images/${card.cardName}_${card._id}_small.jpg`, `./public${card.sImageSrc}`);
            }
            updates.forEach(update => {
                card[update] = req.body[update];
            });
            await card.save();
            res.send();
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.get('/unapproved/:token/page/:pageNum', async (req, res) => {
    const token = req.params.token;

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token });
        if (user.admin) {
            res.render('home', {
                title: 'تایید نشده ها'
            });
        }
    } catch(e) {
        res.status(400).send();
    }
});

router.get('/page/unapprovedCard/:pageNum/:cardPerPage/:nthCard', auth, async (req, res) => {
    const cardPerPage = parseInt(req.params.cardPerPage);
    const cardNum = parseInt(req.params.nthCard);
    const pageNum = parseInt(req.params.pageNum);
    try {
        if (req.user.admin) {
            const cards = await Card.find({ approved: false }).sort([['createdAt', 'descending']])
                .limit(cardPerPage)
                .skip(pageNum * cardPerPage - cardPerPage);
            const card = cards[cardNum];
            res.send({ card, numCards: cards.length});
        }
    } catch(e) {
        res.status(500).send();
    }
});

router.post('/cards/comment/:cardId', auth, async (req, res) => {
    req.body._id = new ObjectID();
    req.body.authorId = req.user._id;
    try {
        if (req.user.activated) {
            const card = await Card.findById(req.params.cardId);
            card.comments = card.comments.concat(req.body);
            await card.save();
            res.send(req.body._id.toHexString());
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.post('/cards/reply/:cardId/:commentId', auth, async (req, res) => {
    req.body._id = new ObjectID();
    req.body.authorId = req.user._id;
    try {
        if (req.user.activated) {
            const card = await Card.findById(req.params.cardId);
            const index = card.comments.findIndex(comment => comment._id.toString() === req.params.commentId);
            card.comments[index].replies = card.comments[index].replies.concat(req.body);
            await card.save();
            res.send(req.body._id.toHexString());
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.get('/readCardComments/:cardId', async (req, res) => {
    try {
        const card = await Card.findById(req.params.cardId);
        res.send(card.comments);
    } catch(e) {
        res.status(400).send(e);
    }
});

router.delete('/cards/reply/:cardId/:commentId/:replyId', auth, async (req, res) => {
    try {
        const card = await Card.findById(req.params.cardId);
        const index = card.comments.findIndex(comment => comment._id.toString() === req.params.commentId);
        const reply = card.comments[index].replies.find(reply => reply._id.toString() === req.params.replyId);
        if (reply.authorId === req.user._id.toString() || req.user.admin === true || card.ownerId === req.user._id.toString()) {
            card.comments[index].replies = card.comments[index].replies.filter(reply => {
                return reply._id.toString() !== req.params.replyId;
            });
            await card.save();
            res.send();
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

router.delete('/cards/comment/:cardId/:commentId', auth, async (req, res) => {
    try {
        const card = await Card.findById(req.params.cardId);
        const comment = card.comments.find(comment => comment._id.toString() === req.params.commentId);
        if (comment.authorId === req.user._id.toString() || req.user.admin === true || card.ownerId === req.user._id.toString()) {
            card.comments = card.comments.filter(comment => {
                return comment._id.toString() !== req.params.commentId;
            });
            await card.save();
            res.send();
        }
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;

