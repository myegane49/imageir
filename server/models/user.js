const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        maxlength: 20,
        required: true,
        unique: true
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        required: true,
        unique: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('آدرس ایمیل معتبر نیست');
            }
        }
    },
    password: {
        type: String,
        trim: true,
        required: true,
        minlength: 6
    },
    activated: {
        type: Boolean,
        default: true
    },
    admin: {
        type: Boolean,
        default: false
    },
    profileImg: {
        type: String,
        default: '/uploads/profileImgs/default-profile-image.jpg'
    },
    saves: [{
        cardID: {
            type: String
        }
    }],
    likes: [{
        cardID: {
            type: String
        }
    }],
    tokens: [{
        token: {
            type: String,
            require: true
        }
    }]
}, {
    timestamps: true
});

// userSchema.virtual('cards', {
//     ref: 'Card',
//     localField: 'name',
//     foreignField: 'ownerName'
// });

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email });
    if (!user) {
        return null;
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        return null;
    }
    
    return user;
};

userSchema.methods.toJSON = function() {
    const user = this;
    const userObject = user.toObject();
    delete userObject.password;
    delete userObject.tokens;
    delete userObject.likes;
    delete userObject.saves;
    
    return userObject;
};

userSchema.methods.generateAuthToken = async function() {
    const user = this;
    const token = jwt.sign({_id: user._id.toString()}, process.env.JWT_SECRET);
    user.tokens = user.tokens.concat({token});
    await user.save();
    return token;
};

userSchema.pre('save', async function(next) {
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;