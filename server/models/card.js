const mongoose = require('mongoose');

const replySchema = new mongoose.Schema({
    authorId: {
        type: String
    },
    reply: {
        type: String
    }
});

const commentSchema = new mongoose.Schema({
    authorId: {
        type: String
    },
    comment: {
        type: String
    },
    replies: [replySchema]
});

const cardSchema = new mongoose.Schema({
    catgTitle: {
        type: String,
        default: 'هوانوردی'
    }, 
    caption: {
        type: String
    }, 
    cardHref: {
        type: String
    },
    sImageSrc: {
        type: String
    },
    lImageSrc: {
        type: String
    },
    cardName: {
        type: String
    },
    imageSize: {
        type: Number
    },
    imageWidth: {
        type: Number
    },
    imageHeight: {
        type: Number
    },
    approved: {
        type: Boolean,
        default: false
    },
    likes: {
        type: Number,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new Error();
            }
        }
    },
    downloads: {
        type: Number,
        default: 0
    },
    comments: [commentSchema],
    ownerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
        // ref: 'User'
    },
    
    // pDate: {
    //     type: String,
    //     required: true
    // }
}, {
    timestamps: true
});

// cardSchema.statics.pagination = (cardsObjArr, eachPage = 3) => {
//     const numPages = Math.ceil(cardsObjArr.length / eachPage);
//     let pagesObjArr = [];
//     for (let i = 0; i < numPages; i++) {
//         pagesObjArr[i] = cardsObjArr.slice(i * eachPage, (i * eachPage) + eachPage);
//     } 
//     return pagesObjArr;
// };

// cardSchema.statics.catConvertor = (eCategory) => {
//     const eCategories = ['technology', 'aviation', 'city', 'food', 'painting', 'animals', 'game', 'space', 'cat', 'dog', 'car', 'nature', 'fantasy', 'texture', 'sports', 'interior'];
//     const pCategories = ['تکنولوژی', 'هوانوردی', 'شهر', 'خوراک', 'نقاشی', 'جانوران', 'بازی', 'فضا', 'گربه', 'سگ', 'خودرو', 'طبیعت', 'فانتزی', 'پس زمینه', 'ورزشی', 'طراحی داخلی'];
//     return pCategories[eCategories.indexOf(eCategory)];
// };

const Card = mongoose.model('Card', cardSchema);

module.exports = Card;